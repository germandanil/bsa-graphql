const { GraphQLServer } = require("graphql-yoga");
const { prisma } = require("./generated/prisma-client");
const Query = require("./resolvers/Query.js");
const Mutation = require("./resolvers/Mutation.js");
const Subscription = require("./resolvers/Subscription.js");
const Message = require("./resolvers/Message.js");
const Reply = require("./resolvers/Reply.js");
const resolvers = {
  Subscription ,
  Reply,
  Message,
  Query,
  Mutation,
};
const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context: { prisma },
});

server.start(() => console.log("Listening on port 4000"));
