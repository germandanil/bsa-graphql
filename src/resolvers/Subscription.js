async function newMessageSubscribe(parent,args,context,info){
    return await context.prisma.$subscribe.message({
        mutation_in:["CREATED","UPDATED"]
    }).node();
}
const newMessage = {
    subscribe: newMessageSubscribe,
    resolve: payload=>{
        return payload
    }
}
async function newReplySubscribe(parent,args,context,info){
    return await context.prisma.$subscribe.reply({
        mutation_in:["CREATED","UPDATED"]
    }).node();
}
const newReply = {
    subscribe: newReplySubscribe,
    resolve: payload=>{
        return payload
    }
}

module.exports = {
    newMessage,
    newReply
}