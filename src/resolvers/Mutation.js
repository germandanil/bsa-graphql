function postMessage(parent,args,context,info){
    return context.prisma.createMessage({
        text: args.text,
        like: 0,
        dislike:0
    })
}

async function postReply(parent,args,context,info){
    const messageExist = await context.prisma.$exists.message({
        id: args.messageId
    })
    if(!messageExist){
        throw new Error(`Message with id ${args.messageId} does not exist`)
    }

    return context.prisma.createReply({
        text:args.text,
        like: 0,
        dislike:0,
        message:{connect:{id: args.messageId}}
    })
}
async function postLike(parent,args,context,info){
    if(args.isMessage){
    return await context.prisma.updateMessage({
        data: {
          like:args.like,
          dislike:args.dislike
        },
        where: { id:args.id },
      })
    }
    return await context.prisma.updateReply({
        data: {
          like:args.like,
          dislike:args.dislike
        },
        where: { id:args.id },
      })
}


module.exports = {
    postReply,
    postMessage,
    postLike
}