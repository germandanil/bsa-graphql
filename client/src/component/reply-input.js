import { useState } from "react";
import { useMutation } from "@apollo/client";
import Input from "./input";

import { ADD_REPLY, MESSAGE_QUERY } from "../query";

export default function Reply({ messageId, isReply }) {
  const [newMessage] = useMutation(ADD_REPLY, {
    refetchQueries: (mutationResult) => [{ query: MESSAGE_QUERY }],
  });
  const [active, setStatus] = useState(false);
  const [text, setText] = useState("");
  function changeStatus() {
    if (!isReply) setStatus(!active);
  }
  function addReply(txt) {
    newMessage({ variables: { messageId, text: txt } });
    setText("");
  }
  if (active)
    return (
      <>
        <button onClick={changeStatus} className="reply-btn">
          Close
        </button>
        <div>
          <Input addMessage={addReply} buttonWord="Reply" />
        </div>
      </>
    );
  else
    return (
      <>
        <button onClick={changeStatus} className="reply-btn">
          Reply
        </button>
      </>
    );
}
