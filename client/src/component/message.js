import Reply from "./reply-input";
import { useMutation } from "@apollo/client";
import { ADD_LIKE, MESSAGE_QUERY } from "../query";

export function Message({ like, dislike, text, id, isReply, replies }) {
  let messageClass = "message";
  if (isReply) messageClass = "reply";
  const [doLike] = useMutation(ADD_LIKE, {
    refetchQueries: (mutationResult) => [{ query: MESSAGE_QUERY }],
  });
  function addLike() {
    doLike({ variables: { like: like + 1, id, dislike, isMessage: !isReply } });
  }
  function addDislike() {
    doLike({
      variables: { like: like, id, dislike: dislike + 1, isMessage: !isReply },
    });
  }

  return (
    <div className={messageClass}>
      <div className="name-wrp">
        <p className="name">#{id.substr(id.length - 3, id.length - 1)}</p>
      </div>
      <div className="text-wrp">
        <p className="text">{text}</p>
      </div>
      <div className="tool-block">
        <div className="like-wrp">
          <i className="fas fa-thumbs-up" onClick={addLike}></i>
          <i className="like-num">{like}</i>
        </div>

        <div className="dislike-wrp">
          <i className="fas fa-thumbs-down" onClick={addDislike}></i>
          <i className="dislike-num">{dislike}</i>
        </div>
      </div>
      <Reply isReply={isReply} messageId={id} />
      <div className="replies">
        {replies.map((r) => (
          <Message
            key={r.id}
            like={r.like}
            dislike={r.dislike}
            id={r.id}
            text={r.text}
            isReply={true}
            replies={[]}
          />
        ))}
      </div>
    </div>
  );
}
