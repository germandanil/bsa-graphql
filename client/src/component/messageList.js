import React from "react";
import { useMemo } from "react";
import { Message } from "./message";
export default function MessageList({ messages }) {
  const messageElements = useMemo(() => {
    return messages.map((m) => {
      return (
        <Message
          key={m.id}
          like={m.like}
          dislike={m.dislike}
          id={m.id}
          text={m.text}
          isReply={false}
          replies={m.replies}
        />
      );
    });
  }, [messages]);
  return <>{messageElements}</>;
}
