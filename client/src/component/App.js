import React, { useEffect } from "react";
import { useMutation, useQuery, useSubscription } from "@apollo/client";

import MessageList from "./messageList";
import Input from "./input";
import { useState } from "react";
import {
  ADD_MESSAGE,
  MESSAGE_QUERY,
  SUBSCRIPTION_ADD_MESSAGE,
  SUBSCRIPTION_REPLY_MESSAGE,
} from "../query";

export default function App() {
  const paginationNum = 3;
  const [filter, setFilter] = useState("");
  const [currentMessages, setCurrentMessages] = useState(paginationNum);
  const [order, setOrder] = useState("createdAt_DESC");
  const { loading, error, data, fetchMore } = useQuery(MESSAGE_QUERY, {
    variables: {
      filter: filter,
      first: currentMessages,
      skip: 0,
      orderBy: order,
    },
    fetchPolicy: "cache-and-network",
  });
  const [newMessage] = useMutation(ADD_MESSAGE, {
    refetchQueries: (mutationResult) => [
      {
        query: MESSAGE_QUERY,
        variables: {
          filter: filter,
          first: currentMessages,
          skip: 0,
          orderBy: order,
        },
      },
    ],
  });

  let content;
  const maxMessages = data ? data.messages.count : 0;
  let messageList = [];

  const { data: SubscrAddMessage } = useSubscription(SUBSCRIPTION_ADD_MESSAGE);
  const { data: SubscrReplyMessage } = useSubscription(
    SUBSCRIPTION_REPLY_MESSAGE
  );
  useEffect(() => {
    fetchMore({
      variables: {
        filter: filter,
        first: currentMessages,
        skip: 0,
        orderBy: order,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        return fetchMoreResult;
      },
    });
  }, [SubscrAddMessage]);
  useEffect(() => {
    console.log(messageList);
    if (data) messageList = data.messages.messageList;
    if (
      messageList.some((r) => {
        console.log(SubscrReplyMessage);
        return r.id === SubscrReplyMessage.newReply.message.id;
      })
    )
      fetchMore({
        variables: {
          filter: filter,
          first: currentMessages,
          skip: 0,
          orderBy: order,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          return fetchMoreResult;
        },
      });
  }, [SubscrReplyMessage]);
  function search(message) {
    setFilter(message);
    fetchMore({
      variables: {
        filter: message,
        first: currentMessages,
        skip: 0,
        orderBy: order,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        return fetchMoreResult;
      },
    });
  }
  function changeOrder(event) {
    setOrder(event.target.value);
    fetchMore({
      variables: {
        filter: filter,
        first: currentMessages,
        skip: 0,
        orderBy: event.target.value,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        return fetchMoreResult;
      },
    });
  }
  function changePagination(event) {
    let newCount = currentMessages + paginationNum;
    if (maxMessages < newCount) newCount = maxMessages;
    setCurrentMessages(newCount);
    fetchMore({
      variables: { filter: filter, first: newCount, skip: 0, orderBy: order },
      updateQuery: (prev, { fetchMoreResult }) => {
        return fetchMoreResult;
      },
    });
  }
  function AddMessageHandler(message) {
    if (message) {
      newMessage({
        variables: { text: message },
      });
    }
  }
  if (loading) content = <div className="loading">'Loading...'</div>;
  else if (error) {
    content = `Error! ${error.message}`;
  } else {
    let messageList = data.messages.messageList;
    content = (
      <>
        <MessageList messages={messageList} />
        {currentMessages !== maxMessages ? (
          <button className="load-more" onClick={changePagination}>
            load more
          </button>
        ) : (
          false
        )}
      </>
    );
  }
  return (
    <div className="chat">
      <div className="tools">
        <Input addMessage={search} buttonWord="search" />
        <div></div>
        <select onChange={changeOrder} value={order}>
          <option value="createdAt_DESC" label="createdAt_DESC" />
          <option value="createdAt_ASC" label="createdAt_ASC" />
          <option value="like_ASC" label="like_ASC" />
          <option value="like_DESC" label="like_DESC" />
          <option value="dislike_ASC" label="dislike_ASC" />
          <option value="dislike_DESC" label="dislike_DESC" />
        </select>
      </div>
      <div className="message-list">{content}</div>
      <Input addMessage={AddMessageHandler} buttonWord="Send" />
    </div>
  );
}
