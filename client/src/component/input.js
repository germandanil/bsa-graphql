import { useState } from "react";

export default function Input({addMessage,buttonWord}){
    const [text,setState] = useState("")

    function handleChange(event) {
      setState( event.target.value);
      if(event.code==="Enter")
      handleSubmit(event)
    }
    function handleEnter(event){
      if (event.key === "Enter") handleSubmit(event);
    }
    function handleSubmit(event) {
      addMessage(text);
      setState("")
      event.preventDefault();
    }
    

      return (
          <div className = "message-input">
            <form onSubmit={handleSubmit}>
            <label>
                <textarea className = "message-input-text" value={text} onChange={handleChange}  onKeyDown={handleEnter}>
                </textarea>
            </label>
            <input className = "message-input-button" type="submit" value={buttonWord}/>
            </form>
        </div>
      );
  }