import { gql } from "@apollo/client";
export const ADD_LIKE = gql`
  mutation PostLike(
    $id: ID!
    $like: Int!
    $dislike: Int!
    $isMessage: Boolean!
  ) {
    postLike(id: $id, like: $like, dislike: $dislike, isMessage: $isMessage) {
      id
    }
  }
`;
export const MESSAGE_QUERY = gql`
  query messagesQuery(
    $filter: String
    $skip: Int
    $first: Int
    $orderBy: MessagesOrderByInput
  ) {
    messages(filter: $filter, skip: $skip, first: $first, orderBy: $orderBy) {
      count
      messageList {
        text
        id
        like
        dislike
        replies {
          id
          text
          like
          dislike
        }
      }
    }
  }
`;

export const ADD_MESSAGE = gql`
  mutation PostMessage($text: String!) {
    postMessage(text: $text) {
      id
    }
  }
`;
export const ADD_REPLY = gql`
  mutation PostReply($messageId: ID!, $text: String!) {
    postReply(text: $text, messageId: $messageId) {
      id
    }
  }
`;
export const SUBSCRIPTION_ADD_MESSAGE = gql`
  subscription {
    newMessage {
      id
    }
  }
`;
export const SUBSCRIPTION_REPLY_MESSAGE = gql`
  subscription {
    newReply {
      message {
        id
      }
    }
  }
`;
